# -*- coding: UTF-8 -*

"""Model : preferences
"""

from app import db


class ScoPreference(db.Model):
    """ScoDoc preferences (par département)"""

    __tablename__ = "sco_prefs"
    id = db.Column(db.Integer, primary_key=True)
    pref_id = db.synonym("id")

    dept_id = db.Column(db.Integer, db.ForeignKey("departement.id"))

    name = db.Column(db.String(128), nullable=False, index=True)
    value = db.Column(db.Text())
    formsemestre_id = db.Column(db.Integer, db.ForeignKey("notes_formsemestre.id"))

    def __repr__(self):
        return f"<{self.__class__.__name__} {self.id} {self.departement.acronym} {self.name}={self.value}>"
