# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""Formulaires gestion des groupes
"""
import flask
from flask import flash, g, render_template, request, url_for

from app.models import FormSemestre, GroupDescr, Partition
from app.models import GROUPNAME_STR_LEN
from app.scodoc import html_sco_header
from app.scodoc.sco_exceptions import AccessDenied
import app.scodoc.sco_utils as scu
from app.scodoc.TrivialFormulator import TrivialFormulator


def affect_groups(partition_id):
    """Formulaire affectation des etudiants aux groupes de la partition.
    Permet aussi la creation et la suppression de groupes.
    """
    # réécrit pour 9.0.47 avec un template
    partition = Partition.query.get_or_404(partition_id)
    formsemestre = partition.formsemestre
    if not formsemestre.can_change_groups():
        raise AccessDenied("vous n'avez pas la permission de modifier les groupes")
    formsemestre.setup_parcours_groups()
    return render_template(
        "scolar/affect_groups.j2",
        sco_header=html_sco_header.sco_header(
            page_title="Affectation aux groupes",
            javascripts=["js/groupmgr.js"],
            cssstyles=["css/groups.css"],
        ),
        sco_footer=html_sco_header.sco_footer(),
        partition=partition,
        # Liste des partitions sans celle par defaut:
        partitions_list=partition.formsemestre.partitions.filter(
            Partition.partition_name != None
        ),
        formsemestre_id=formsemestre.id,
    )


def group_rename(group_id):
    """Form to rename a group"""
    group: GroupDescr = GroupDescr.query.get_or_404(group_id)
    formsemestre_id = group.partition.formsemestre_id
    formsemestre = FormSemestre.get_formsemestre(formsemestre_id)
    if not formsemestre.can_change_groups():
        raise AccessDenied("Vous n'avez pas le droit d'effectuer cette opération !")
    H = [f"<h2>Renommer un groupe de {group.partition.partition_name or '-'}</h2>"]
    tf = TrivialFormulator(
        request.base_url,
        scu.get_request_args(),
        (
            ("group_id", {"default": group_id, "input_type": "hidden"}),
            (
                "group_name",
                {
                    "title": "Nouveau nom",
                    "default": group.group_name,
                    "size": 12,
                    "allow_null": False,
                    "validator": lambda val, _: len(val) < GROUPNAME_STR_LEN,
                    "explanation": "doit être unique dans cette partition"
                    if group.partition.groups_editable
                    else "groupes non modifiables dans cette partition",
                    "enabled": group.partition.groups_editable,
                },
            ),
            (
                "edt_id",
                {
                    "title": "Id EDT",
                    "default": group.edt_id or "",
                    "size": 12,
                    "allow_null": True,
                    "explanation": """optionnel : identifiant du groupe dans le logiciel
                        d'emploi du temps, pour le cas où les noms de groupes ne seraient pas
                        les mêmes dans ScoDoc et dans l'emploi du temps (si plusieurs ids de
                        groupes EDT doivent correspondre au même groupe ScoDoc,
                        les séparer par des virgules).""",
                },
            ),
        ),
        submitlabel="Enregistrer",
        cancelbutton="Annuler",
    )
    dest_url = url_for(
        "scolar.partition_editor",
        scodoc_dept=g.scodoc_dept,
        formsemestre_id=group.partition.formsemestre_id,
        edit_partition=1,
    )
    if tf[0] == 0:
        return (
            html_sco_header.sco_header()
            + "\n".join(H)
            + "\n"
            + tf[1]
            + html_sco_header.sco_footer()
        )
    elif tf[0] == -1:
        return flask.redirect(dest_url)
    else:
        # form submission
        # Si la partition n'est pas editable, on ne peut changer que l'edt_id
        group.set_edt_id(tf[2]["edt_id"])
        if group.partition.groups_editable:
            group.set_name(tf[2]["group_name"], dest_url=dest_url)
        flash("groupe modifié")
        return flask.redirect(dest_url)
