##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""Jury BUT: calcul des décisions de jury annuelles "automatiques"
"""
from flask import g, url_for

from app import db
from app.but import jury_but
from app.models import Identite, FormSemestre, ScolarNews
from app.scodoc import sco_cache
from app.scodoc.sco_exceptions import ScoValueError


def formsemestre_validation_auto_but(
    formsemestre: FormSemestre, only_adm: bool = True
) -> int:
    """Calcul automatique des décisions de jury sur une "année" BUT.

    - N'enregistre jamais de décisions de l'année scolaire précédente, même
    si on a des RCUE "à cheval".
    - Normalement, only_adm est True et on n'enregistre que les décisions validantes
    de droit: ADM ou CMP.
    En revanche, si only_adm est faux, on enregistre la première décision proposée par ScoDoc
    (mode à n'utiliser que pour les tests unitaires vérifiant la saisie des jurys)

    Returns: nombre d'étudiants pour lesquels on a enregistré au moins un code.
    """
    if not formsemestre.formation.is_apc():
        raise ScoValueError("fonction réservée aux formations BUT")
    nb_etud_modif = 0
    with sco_cache.DeferredSemCacheManager():
        for etudid in formsemestre.etuds_inscriptions:
            etud = Identite.get_etud(etudid)
            deca = jury_but.DecisionsProposeesAnnee(etud, formsemestre)
            nb_etud_modif += deca.record_all(only_validantes=only_adm)

    db.session.commit()
    ScolarNews.add(
        typ=ScolarNews.NEWS_JURY,
        obj=formsemestre.id,
        text=f"""Calcul jury automatique du semestre {formsemestre.html_link_status()}""",
        url=url_for(
            "notes.formsemestre_status",
            scodoc_dept=g.scodoc_dept,
            formsemestre_id=formsemestre.id,
        ),
    )
    return nb_etud_modif
