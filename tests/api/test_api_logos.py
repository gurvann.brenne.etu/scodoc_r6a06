#!/usr/bin/env python3
# -*- mode: python -*-
# -*- coding: utf-8 -*-

"""Exemple utilisation API ScoDoc 9 avec jeton obtenu par basic authentication

utilisation:
    à faire fonctionner en environnment de test (FLASK_ENV=test_api dans le fichier .env)
    pytest tests/api/test_api_logos.py
"""


# XXX TODO
#  Ce test a une logique très différente des autres : A UNIFIER

import requests

from app.scodoc import sco_utils as scu
from scodoc import app
from tests.api.setup_test_api import (
    API_URL,
    api_admin_headers,
    api_headers,
    CHECK_CERTIFICATE,
)

from tests.unit.config_test_logos import (
    create_super_token,
    create_admin_token,
    create_lambda_token,
    create_logos,
    create_dept,
)


def test_super_access(api_admin_headers):
    """
    Route: /logos
    """
    response = requests.get(
        API_URL + "/logos",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 200
    assert response.json() is not None


def test_lambda_access(api_headers):
    """
    Route: /logos
    """
    response = requests.get(
        API_URL + "/logos",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 401


# XXX A REVOIR
def test_global_logos(api_admin_headers):
    """
    Route:
    """
    response = requests.get(
        API_URL + "/logos",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 200
    assert response.json() is not None
    assert "header" in response.json()
    # assert "footer" in response.json()  # XXX ??? absent
    assert "B" in response.json()
    assert "C" in response.json()


def test_local_by_id_logos(api_admin_headers):
    """
    Route: /departement/id/1/logos
    """
    response = requests.get(
        API_URL + "/departement/id/1/logos",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 200
    assert response.json() is not None
    assert "A" in response.json()
    assert "D" in response.json()


def test_local_by_name_logos(api_admin_headers):
    """
    Route: /departement/TAPI/logos
    """
    response = requests.get(
        API_URL + "/departement/TAPI/logos",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
    )
    assert response.status_code == 200
    assert response.json() is not None
    assert "A" in response.json()
    assert "D" in response.json()


def test_local_png_by_id_logo(api_admin_headers):
    """
    Route: /departement/id/1/logo/D
    """
    response = requests.get(
        API_URL + "/departement/id/1/logo/D",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "image/png"
    assert response.headers["Content-Disposition"].startswith("inline")
    assert "logo_D.png" in response.headers["Content-Disposition"]


def test_global_png_logo(api_admin_headers):
    """
    Route: /logo/C
    """
    response = requests.get(
        API_URL + "/logo/C",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "image/png"
    assert response.headers["Content-Disposition"].startswith("inline")
    assert "logo_C.png" in response.headers["Content-Disposition"]


def test_global_jpg_logo(api_admin_headers):
    """
    Route: /logo/B
    """
    response = requests.get(
        API_URL + "/logo/B",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "image/jpg"
    assert response.headers["Content-Disposition"].startswith("inline")
    assert "logo_B.jpg" in response.headers["Content-Disposition"]


def test_local_png_by_name_logo(api_admin_headers):
    """
    Route: /departement/TAPI/logo/D
    """
    response = requests.get(
        API_URL + "/departement/TAPI/logo/D",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "image/png"
    assert response.headers["Content-Disposition"].startswith("inline")
    assert "logo_D.png" in response.headers["Content-Disposition"]


def test_local_jpg_by_id_logo(api_admin_headers):
    """
    Route: /departement/id/1/logo/A
    """
    response = requests.get(
        API_URL + "/departement/id/1/logo/A",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "image/jpg"
    assert response.headers["Content-Disposition"].startswith("inline")
    assert "logo_A.jpg" in response.headers["Content-Disposition"]


def test_local_jpg_by_name_logo(api_admin_headers):
    """
    Route: /departement/TAPI/logo/A
    """
    response = requests.get(
        API_URL + "/departement/TAPI/logo/A",
        headers=api_admin_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "image/jpg"
    assert response.headers["Content-Disposition"].startswith("inline")
    assert "logo_A.jpg" in response.headers["Content-Disposition"]
