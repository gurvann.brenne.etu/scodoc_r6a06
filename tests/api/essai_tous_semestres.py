#!/usr/bin/env python3
# -*- mode: python -*-
# -*- coding: utf-8 -*-

"""Exemple utilisation API ScoDoc 9 avec jeton obtenu par basic authentication

    Usage:
        cd /opt/scodoc/tests/api
        python -i essai_tous_semestres.py

    Ce script va demander les semestres suivi par chaque étudiant !
"""


from pprint import pprint as pp
import sys
import urllib3
from setup_test_api import (
    API_PASSWORD,
    API_URL,
    API_USER,
    APIError,
    CHECK_CERTIFICATE,
    get_auth_headers,
    GET,
    POST_JSON,
    SCODOC_URL,
)


if not CHECK_CERTIFICATE:
    urllib3.disable_warnings()

print(f"SCODOC_URL={SCODOC_URL}")
print(f"API URL={API_URL}")


HEADERS = get_auth_headers(API_USER, API_PASSWORD)


# Liste de tous les étudiants en cours (de tous les depts)
etuds = GET("/etudiants/courants", headers=HEADERS)

print(f"{len(etuds)} étudiants courants")
nb_etuds = 0
nb_sems = 0
nb_empty_sems = 0
for etud in etuds:
    if etud["code_nip"]:
        sems = GET(f"/etudiant/nip/{etud['code_nip']}/formsemestres", headers=HEADERS)
        assert not sems or [sem["annee_scolaire"] for sem in sems]
        nb_etuds += 1
        nb_sems += len(sems)
        if not sems:
            nb_empty_sems += 1

print(
    f"testé {nb_etuds} etuds avec NIP, et {nb_sems} semestres (dont {nb_empty_sems} vides)."
)
