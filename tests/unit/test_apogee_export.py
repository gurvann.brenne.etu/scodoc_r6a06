##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

""" Test export Apogéee

Ces tests sont généralement lents (construction de la base),
et donc marqués par `@pytest.mark.slow`.

Certains sont aussi marqués par @pytest.mark.lemans ou @pytest.mark.lyon
pour lancer certains tests spécifiques seulement.

Exemple utilisation spécifique:
# test sur "apo" seulement:
pytest --pdb -m apo tests/unit/test_apogee_export.py

Elements Apogée simulés:

- UEs : TIU2x
- Ressources: R2.xy : TIRxy  (VRETR201 -> TIR201)
"""

import pytest
from tests.unit import yaml_setup, yaml_setup_but

import app
from app.but.jury_but_validation_auto import formsemestre_validation_auto_but
from app.models import Formation, FormSemestre, UniteEns
from config import TestConfig

DEPT = TestConfig.DEPT_TEST


@pytest.mark.skip  # Ce "test" est utilisé comme setup pour développer, pas comme test unitaire routinier
@pytest.mark.slow
@pytest.mark.apo
def test_refcomp_niveaux_info(test_client):
    """Test niveaux / parcours / UE pour un BUT INFO
    avec parcours A et B, même compétences mais coefs différents
    selon le parcours.
    """
    # WIP
    # pour le moment juste le chargement de la formation, du ref. comp, et des UE du S4.
    app.set_sco_dept(DEPT)
    doc, formation, formsemestre_titres = yaml_setup.setup_from_yaml(
        "tests/ressources/yaml/cursus_but_info.yaml"
    )
    for formsemestre_titre in formsemestre_titres:
        formsemestre = yaml_setup.create_formsemestre_with_etuds(
            doc, formation, formsemestre_titre
        )
    #
