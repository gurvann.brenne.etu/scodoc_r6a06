XX-APO_TITRES-XX
apoC_annee	2021/2022
apoC_cod_dip	DIPTIS2
apoC_Cod_Exp	2
apoC_cod_vdi	17
apoC_Fichier_Exp	export.txt
apoC_lib_dip	BUT INFO TEST
apoC_Titre1	Maquette pour tests unitaires sur un BUT Info S2
apoC_Titre2	
							
XX-APO_TYP_RES-XX
10	AB1	AB2	ABI	ABJ	ADM	AJ	AJRO	C1	DEF	DIF	
18	AB1	AB2	ABI	ABJ	ADM	ADMC	ADMD	AJ	AJAC	AJAR	AJRO	ATT	B1	C1	COMP	DEF	DIF	NAR	
45	ABI	ABJ	ADAC	ADM	ADMC	ADMD	AIR	AJ	AJAR	AJCP	AJRO	AJS	ATT	B1	B2	C1	COMP	CRED	DEF	DES	DETT	DIF	ENER	ENRA	EXC	INFA	INFO	INST	LC	MACS	N1	N2	NAR	NON	NSUI	NVAL	OUI	SUIV	SUPS	TELE	TOEF	TOIE	VAL	VALC	VALR	
10	ABI	ABJ	ADMC	COMP	DEF	DIS	NVAL	VAL	VALC	VALR	
AB1 : Ajourné en B2 mais admis en B1	AB2 : ADMIS en B1 mais ajourné en B2	ABI : Absence	ABJ : Absence justifiée	ADM : Admis	AJ : Ajourné	AJRO : Ajourné - Réorientation Obligatoire	C1 : Niveau C1	DEF : Défaillant	DIF : Décision différée	
AB1 : Ajourné en B2 mais admis en B1	AB2 : ADMIS en B1 mais ajourné en B2	ABI : Absence	ABJ : Absence justifiée	ADM : Admis	ADMC : Admis avec compensation	ADMD : Admis (passage avec dette)	AJ : Ajourné	AJAC : Ajourné mais accès autorisé à étape sup.	AJAR : Ajourné et Admis A Redoubler	AJRO : Ajourné - Réorientation Obligatoire	ATT : En attente de décison	B1 : Niveau B1	C1 : Niveau C1	COMP : Compensé	DEF : Défaillant	DIF : Décision différée	NAR : Ajourné non admis à redoubler	
ABI : Absence	ABJ : Absence justifiée	ADAC : Admis avant choix	ADM : Admis	ADMC : Admis avec compensation	ADMD : Admis (passage avec dette)	AIR : Ingénieur spécialité Informatique appr	AJ : Ajourné	AJAR : Ajourné et Admis A Redoubler	AJCP : Ajourné mais autorisé à compenser	AJRO : Ajourné - Réorientation Obligatoire	AJS : Ajourné (note éliminatoire)	ATT : En attente de décison	B1 : Niveau B1	B2 : Niveau B2	C1 : Niveau C1	COMP : Compensé	CRED : Eléments en crédits	DEF : Défaillant	DES : Désistement	DETT : Eléments en dettes	DIF : Décision différée	ENER : Ingénieur spécialité Energétique	ENRA : Ingénieur spécialité Energétique appr	EXC : Exclu	INFA : Ingénieur spécialité Informatique appr	INFO : Ingénieur spécialié Informatique	INST : Ingénieur spécialité Instrumentation	LC : Liste complémentaire	MACS : Ingénieur spécialité MACS	N1 : Compétences CLES	N2 : Niveau N2	NAR : Ajourné non admis à redoubler	NON : Non	NSUI : Non suivi(e)	NVAL : Non Validé(e)	OUI : Oui	SUIV : Suivi(e)	SUPS : Supérieur au seuil	TELE : Ingénieur spéciailté Télécommunications	TOEF : TOEFL	TOIE : TOEIC	VAL : Validé(e)	VALC : Validé(e) par compensation	VALR : Validé(e) Retrospectivement	
ABI : Absence	ABJ : Absence justifiée	ADMC : Admis avec compensation	COMP : Compensé	DEF : Défaillant	DIS : Dispense examen	NVAL : Non Validé(e)	VAL : Validé(e)	VALC : Validé(e) par compensation	VALR : Validé(e) Retrospectivement	
							
XX-APO_COLONNES-XX
apoL_a01_code	Type Objet	Code	Version	Année	Session	Admission/Admissibilité	Type Rés.			Etudiant	Numéro
apoL_a02_nom											Nom
apoL_a03_prenom											Prénom
apoL_a04_naissance									Session	Admissibilité	Naissance
APO_COL_VAL_DEB
apoL_c0001	ELP	V1INFU21		2021	0	1	N	V1INFU21 - UE 2.1 Réaliser	0	1	Note
apoL_c0002	ELP	V1INFU21		2021	0	1	B		0	1	Barème
apoL_c0003	ELP	V1INFU21		2021	0	1	J		0	1	Pts Jury
apoL_c0004	ELP	V1INFU21		2021	0	1	R		0	1	Résultat
apoL_c0005	ELP	V1INFU22		2021	0	1	N	V1INFU22 - UE 2.2 Optimiser	0	1	Note
apoL_c0006	ELP	V1INFU22		2021	0	1	B		0	1	Barème
apoL_c0007	ELP	V1INFU22		2021	0	1	J		0	1	Pts Jury
apoL_c0008	ELP	V1INFU22		2021	0	1	R		0	1	Résultat
apoL_c0009	ELP	V1INFU23		2021	0	1	N	V1INFU23 - UE 2.3 Administrer	0	1	Note
apoL_c0010	ELP	V1INFU23		2021	0	1	B		0	1	Barème
apoL_c0011	ELP	V1INFU23		2021	0	1	J		0	1	Pts Jury
apoL_c0012	ELP	V1INFU23		2021	0	1	R		0	1	Résultat
apoL_c0013	ELP	V1INFU24		2021	0	1	N	V1INFU24 - UE 2.4 Gérer	0	1	Note
apoL_c0014	ELP	V1INFU24		2021	0	1	B		0	1	Barème
apoL_c0015	ELP	V1INFU24		2021	0	1	J		0	1	Pts Jury
apoL_c0016	ELP	V1INFU24		2021	0	1	R		0	1	Résultat
apoL_c0017	ELP	V1INFU25		2021	0	1	N	V1INFU25 - UE 2.5 Conduire	0	1	Note
apoL_c0018	ELP	V1INFU25		2021	0	1	B		0	1	Barème
apoL_c0019	ELP	V1INFU25		2021	0	1	J		0	1	Pts Jury
apoL_c0020	ELP	V1INFU25		2021	0	1	R		0	1	Résultat
apoL_c0021	ELP	V1INFU26		2021	0	1	N	V1INFU26 - UE 2.6 Travailler	0	1	Note
apoL_c0022	ELP	V1INFU26		2021	0	1	B		0	1	Barème
apoL_c0023	ELP	V1INFU26		2021	0	1	J		0	1	Pts Jury
apoL_c0024	ELP	V1INFU26		2021	0	1	R		0	1	Résultat
apoL_c0025	ELP	VINFR201		2021	0	1	N	VINFR201 - Développement orienté objets	0	1	Note
apoL_c0026	ELP	VINFR201		2021	0	1	B		0	1	Barème
apoL_c0027	ELP	VINFR207		2021	0	1	N	VINFR207 - Graphes	0	1	Note
apoL_c0028	ELP	VINFR207		2021	0	1	B		0	1	Barème
apoL_c0029	ELP	VINFPOR2		2021	0	1	N	VINFPOR2 - Portfolio	0	1	Note
apoL_c0030	ELP	VINFPOR2		2021	0	1	B		0	1	Barème
apoL_c0031	ELP	TIRW2		2021	0	1	N	TIRW2 - Semestre 2 BUT INFO 2	0	1	Note
apoL_c0032	ELP	TIRW2		2021	0	1	B		0	1	Barème
apoL_c0033	ELP	TIRW2		2021	0	1	J		0	1	Pts Jury
apoL_c0034	ELP	TIRW2		2021	0	1	R		0	1	Résultat
apoL_c0035	ELP	TIRO		2021	0	1	N	TIRO - Année BUT 1 RT	0	1	Note
apoL_c0036	ELP	TIRO		2021	0	1	B		0	1	Barème
apoL_c0037	VET	TI1	117	2021	0	1	N	TI1 - BUT INFO an1	0	1	Note
apoL_c0038	VET	TI1	117	2021	0	1	B		0	1	Barème
apoL_c0039	VET	TI1	117	2021	0	1	J		0	1	Pts Jury
apoL_c0040	VET	TI1	117	2021	0	1	R		0	1	Résultat
APO_COL_VAL_FIN
apoL_c0041	APO_COL_VAL_FIN							  	
							
XX-APO_VALEURS-XX
apoL_a01_code	apoL_a02_nom	apoL_a03_prenom	apoL_a04_naissance	apoL_c0001	apoL_c0002	apoL_c0003	apoL_c0004	apoL_c0005	apoL_c0006	apoL_c0007	apoL_c0008	apoL_c0009	apoL_c0010	apoL_c0011	apoL_c0012	apoL_c0013	apoL_c0014	apoL_c0015	apoL_c0016	apoL_c0017	apoL_c0018	apoL_c0019	apoL_c0020	apoL_c0021	apoL_c0022	apoL_c0023	apoL_c0024	apoL_c0025	apoL_c0026	apoL_c0027	apoL_c0028	apoL_c0029	apoL_c0030	apoL_c0031	apoL_c0032	apoL_c0033	apoL_c0034	apoL_c0035	apoL_c0036	apoL_c0037	apoL_c0038	apoL_c0039	apoL_c0040

1001	ex_a1	Jean	10/01/2003																																								
1002	ex_a2	Lucie	11/01/2003																																								
1003	ex_b1	Hélène	11/01/2003																																								
1004	ex_b2	Rose	11/01/2003																																								
